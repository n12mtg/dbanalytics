CREATE VIEW bigsettings AS
SELECT
run_id,time_elapsed_id,tension_set,velocity_set,Control_Setpoints.row_update_time,

A.entry_type AS A_entry_type,A.H2 AS A_H2,A.C2H4 AS A_C2H4,A.C2H2 AS A_C2H2,A.Ar AS A_Ar,A.CO2 AS A_CO2,

D.entry_type AS D_entry_type,D.H2 AS D_H2,D.Ar_H2O AS D_Ar_H2O,D.Ar AS D_Ar,
G.H2 AS G_H2,G.C2H4 AS G_C2H4,G.C2H2 AS G_C2H2,G.Ar AS G_Ar,G.CO2 AS G_CO2,

I.entry_type AS I_entry_type,I.entrance_airknife AS I_entrance_airknife,I.entrance_iso AS I_entrance_iso,I.exit_airknife AS I_exit_airknife,I.exit_iso AS I_exit_iso,I.chamber_purge AS I_chamber_purge,I.pyro_manifold AS I_pyro_manifold,

H1.entry_type AS H1_entry_type,H1.station_num AS H1_station_num,H1.station_type AS H1_station_type,H1.set_point AS H1_set_point,H1.t_op AS H1_t_op,H1.temp AS H1_temp,H2.entry_type AS H2_entry_type,

H2.station_num AS H2_station_num,H2.station_type AS H2_station_type,H2.set_point AS H2_set_point,H2.t_op AS H2_t_op,H2.temp AS H2_temp,

H3.entry_type AS H3_entry_type,H3.station_num AS H3_station_num,H3.station_type AS H3_station_type,H3.set_point AS H3_set_point,H3.t_op AS H3_t_op,H3.temp AS H3_temp,

H4.entry_type AS H4_entry_type,H4.station_num AS H4_station_num,H4.station_type AS H4_station_type,H4.set_point AS H4_set_point,H4.t_op AS H4_t_op,H4.temp AS H4_temp,

H5.entry_type AS H5_entry_type,H5.station_num AS H5_station_num,H5.station_type AS H5_station_type,H5.set_point AS H5_set_point,H5.t_op AS H5_t_op,H5.temp AS H5_temp
FROM Control_Setpoints
JOIN Anneal_MFC_Setpoints AS A ON Control_Setpoints.anneal_id = A.id
JOIN Delam_MFC_Setpoints AS D ON Control_Setpoints.delam_id = D.id
JOIN Growth_MFC_Setpoints AS G ON Control_Setpoints.growth_id = G.id
JOIN Inert_MFC_Setpoints AS I ON Control_Setpoints.inert_id = I.id
JOIN Heat_Station_Setpoints AS H1 ON Control_Setpoints.heat_station_1_id = H1.id
JOIN Heat_Station_Setpoints AS H2 ON Control_Setpoints.heat_station_2_id = H2.id
JOIN Heat_Station_Setpoints AS H3 ON Control_Setpoints.heat_station_3_id = H3.id
JOIN Heat_Station_Setpoints AS H4 ON Control_Setpoints.heat_station_4_id = H4.id
JOIN Heat_Station_Setpoints AS H5 ON Control_Setpoints.heat_station_5_id = H5.id;